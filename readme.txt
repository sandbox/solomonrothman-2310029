INTRODUCTION
------------
This module requires the Disqus (https://www.drupal.org/project/disqus) module and is only usefull if you're using the SSO (Single Sign On) feature.
For now it adds a more robust user avatar check (compatible with file entity) to make sure that disqus SSO gets the correct user avatar for comments. It also allows you to set an image style for the avatar. Disqus recommends sending a 128X128 picture, so this allows for better cropping and a potentially reduced file size (some sites have large profile photos) when Disqus requests and downloads the image.

* For a full description of the module, visit the project page:
   hhttps://www.drupal.org/sandbox/solomonrothman/2310029
* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2310029
   
REQUIREMENTS
------------
This module requires the following modules:
 * Disqus (https://www.drupal.org/project/disqus)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure general settings at /admin/config/services/disqus/user-avatars and follow the instructions on that screen.
 
 MAINTAINERS
 -----------
 Current maintainers:
 * Solomon Rothman - https://www.drupal.org/u/solomonrothman